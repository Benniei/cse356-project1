var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

const port = 3000;
const hostname = '209.94.58.237';

app.set("view engine", "ejs")
app.use(express.static(path.join(__dirname, "public")));

app.get("/ttt/", (req, res) => {
    res.sendFile('./frontpage.html', { root: __dirname });
});

app.post("/ttt/", (req, res) => {
    const { name } = req.body;
    let dateNow = new Date().toLocaleDateString()
    res.render("hello", {
        name: name,
        date: dateNow
    });
});

app.post("/ttt/play", (req, res) => {
    const { grid } = req.body;

    var winner;

    if (grid[0] !== " " && grid[0] === grid[1] && grid[0] === grid[2]) {
        winner = grid[0];
    } else if (grid[3] !== " " && grid[3] === grid[4] && grid[3] === grid[5]) {
        winner = grid[3];
    } else if (grid[6] !== " " && grid[6] === grid[7] && grid[6] === grid[8]) {
        winner = grid[6];
    } else if (grid[0] !== " " && grid[0] === grid[3] && grid[0] === grid[6]) {
        winner = grid[0];
    } else if (grid[1] !== " " && grid[1] === grid[4] && grid[1] === grid[7]) {
        winner = grid[1];
    } else if (grid[2] !== " " && grid[2] === grid[5] && grid[2] === grid[8]) {
        winner = grid[2];
    } else if (grid[0] !== " " && grid[0] === grid[4] && grid[0] === grid[8]) {
        winner = grid[0];
    } else if (grid[2] !== " " && grid[2] === grid[4] && grid[2] === grid[6]) {
        winner = grid[2];
    } else {
        winner = " ";
    }
    const result = {
        grid: grid,
        winner: winner
    }
    res.send(result);
});

app.listen(port, hostname, () => {
    console.log("server running on 3000")
});